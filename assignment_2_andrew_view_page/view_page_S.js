// codes runs while the page load
let id = ""
let map = ''
let arrivallat = '';
let arrivallong = '';
let departurelat = '';
let departurelong = '';
let marker1 = null;
let marker2 = null;
function localStorage () {
    let indOfTrip = JSON.parse(localStorage.getItem("TRIPS_ARRAY_INDEX"))
    let indOfArrayTrip = JSON.parse(localStorage.getItem("TRIPS_ARRAY_KEY"))._TripListArray[indOfTrip]
    arrivallat = indOfArrayTrip._ArrivalLat
    arrivallong = indOfArrayTrip._ArrivalLong
    departurelat = indOfArrayTrip._DeparLat
    departurelong = indOfArrayTrip._DeparLong
    
    let id = indOfArrayTrip._id
    let startDeparture = indOfArrayTrip._StartDeparture
    let destination = indOfArrayTrip._Destination
    let stopoverDestination = indOfArrayTrip._StopoverDestination
    let carModel = indOfArrayTrip._CarModel
    let distance = indOfArrayTrip._Distance
    let estimatePrice = indOfArrayTrip._EstimatePrice
    document.getElementById("Start_Departure").innerHTML = startDeparture
    document.getElementById("Destination").innerHTML = destination
    document.getElementById("Stopover_Destination").innerHTML = stopoverDestination
    document.getElementById("Car_Model").innerHTML = carModel    
    document.getElementById("Distance").innerHTML = distance
    document.getElementById("Estimate_Price").innerHTML = estimatePrice
    
    mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 5,
        center: [arrivallong, arrivallat]
    })

    marker1 = new mapboxgl.Marker({
        color: "red",
        draggable: false
    }).setLngLat([arrivallong, arrivallat])
        .addTo(map)
    marker2 = new mapboxgl.Marker({
        color: "blue",
        draggable: false
    }).setLngLat([departurelong, departurelat])
        .addTo(map)

    setTimeout(function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [[departurelong, departurelat], [arrivallong, arrivallat]]
                }
            }
        })
    }, 3000);

}

function backToHome () {
    window.location = "填写需要连接的html文件.html"
}

function cancel () {
    if (confirm("Are you sure to cancel and delete the specific details and information of this trip?")) {
        创造的class名字.class里面的methods其目的是删除此次行程(id);
        创造一个能够更新local storage的函数(Triplist);
        alert("This trip has been deleted and will return to 某些 page.");
        window.location = "填写需要连接的html文件.html"
    }
}

function showRoute () {
    mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 5,
        center: [arrivallong, arrivallat]
    })

    marker1 = new mapboxgl.Marker({
        color: "green",
        draggable: false
    }).setLngLat([arrivallong, arrivallat])
        .addTo(map)
    marker2 = new mapboxgl.Marker({
        color: "black",
        draggable: false
    }).setLngLat([departurelong, departurelat])
        .addTo(map)

    setTimeout(function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [[departurelong, departurelat], [arrivallong, arrivallat]]
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        })
    },1000);
}

    setTimeout(localStorage(), 3000)