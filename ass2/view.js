// codes runs while the page load
let arrivallat = '';
let arrivallong = '';
let departurelat = '';
let departurelong = '';
let indOfTrip = localStorage.getItem("TRIP_INDEX_KEY")
    let trip= account.getTrip(indOfTrip);

//this function is used to display information of the trip, also contain marker on the map
function localStorage () {
    arrivallat = trip.start.coordinates[1]
    arrivallong = trip.start.coordinates[0]
    departurelat = trip.desination.coordinates[1]
    departurelong = trip.desination.coordinates[0]
    
    let startDeparture = trip.start.name
    let destination = trip.desination.name
    let stopoverDestination = trip.stops
    let carModel = trip.taxiType
    let distance = trip.distance
    let estimatePrice = trip.price
    document.getElementById("Start_Departure").innerHTML = startDeparture
    document.getElementById("Destination").innerHTML = destination
    document.getElementById("Stopover_Destination").innerHTML = stopoverDestination
    document.getElementById("Car_Model").innerHTML = carModel    
    document.getElementById("Distance").innerHTML = distance
    document.getElementById("Estimate_Price").innerHTML = estimatePrice
    
    mapboxgl.accessToken = 'pk.eyJ1IjoiamVycnl3ZWkiLCJhIjoiY2tzanI4anU0MDUwMDJubXRpd2xka3d6aCJ9.NUKoTBVodGow14Z9vig1UA';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 5,
        center: [arrivallong, arrivallat]
    })

    marker1 = new mapboxgl.Marker({
        color: "red",
        draggable: false
    }).setLngLat([arrivallong, arrivallat])
        .addTo(map)
    marker2 = new mapboxgl.Marker({
        color: "blue",
        draggable: false
    }).setLngLat([departurelong, departurelat])
        .addTo(map)

    setTimeout(function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [[departurelong, departurelat], [arrivallong, arrivallat]]
                }
            }
        })
    }, 3000)
}
//go the the home page
function backToHome () {
    window.location = "main.html"
}
// this function will be called when push the button Cancel, and it need a if statement to check the time, it only can cancel the future trips
function cancel () {  
    let timeNow=new Date.getTime()
    if(new Date(trip.time).getTime()>timeNow){
        if (confirm("Are you sure to cancel and delete the specific details and information of this trip?")) {
            account.removeTrip(index)
            updateLocalStorage(APP_DATA_KEY,tripList);
            alert("This trip has been deleted and will return to main page.");
            window.location = "main.html"
        }
        else{
            alert('you can not delete past trip')
        }
}
//this function is used to show the route between markers
function showRoute () {
    mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubGlueHUiLCJhIjoiY2tudHBjdzEwMDRidjJ2czcwY3M0NGJzdCJ9.s8wnc3vCmr9TGOIhz5Syiw';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 5,
        center: [arrivallong, arrivallat]
    })

    marker1 = new mapboxgl.Marker({
        color: "green",
        draggable: false
    }).setLngLat([arrivallong, arrivallat])
        .addTo(map)
    marker2 = new mapboxgl.Marker({
        color: "black",
        draggable: false
    }).setLngLat([departurelong, departurelat])
        .addTo(map)

    setTimeout(function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [[departurelong, departurelat], [arrivallong, arrivallat]]
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        })
    },1000);
}

    setTimeout(localStorage(), 3000)