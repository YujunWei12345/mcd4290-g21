"use strict"
//this is used for past page

//define a global variable to store past trips
let pastTripList=[]

//for loop to check all the trip,if statement to compare all the time with current time, if trips less than the current time,  this trips push into pastTripList.
for(let i=0;i<tripList.length;i++){
    let timeNow=new Date.getTime()
    if(new Date(tripList[i].time).getTime()<timeNow){
        pastTripList.push(tripList[i])
    }
}

//this function is used to display every past trip's information on the past page.
/*
function displayPastTrip(list)
use a for loop, counter is the list
copy a html code, the variables need to display are changing to the corresponding value with i
*/
function displayPastTrip(list){
    for(let i=0;i<list.length;i++){
        let displayHtml=''
        displayHtml+='<div class="mdl-cell mdl-cell--3-col"><div class="demo-card-wide mdl-card mdl-shadow--2dp"><div class="mdl-card__title"><h2 class="mdl-card__title-text">Trip Date:'+list[i].time+'</h2></div><div class="mdl-card__actions mdl-card--border">s:'+list[i].start+'-'+list[i].stops+'-'+list[i].destination+'<br><br>taxi type:'+list[i].taxiType+'<br>distance:'+list[i].distance+'<br>price:'+list[i].price+'<br><a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view('+i+')">View Trip</a></div><div class="mdl-card__menu"></div></div></div>'
    }
    document.getElementById('displayArea').innerHTML=displayHtml
}

//this function will be called when click the view button, update the localstorage first, and then go the view page.
function view(index){
    updateLocalStorage(TRIP_INDEX_KEY,index)
    window.location('view.html')
}

//call the function displayPastTrip with the input pastTripList
displayPastTrip(pastTripList)