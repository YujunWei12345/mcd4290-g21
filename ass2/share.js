const TRIP_INDEX_KEY = "tripIndex";
const APP_DATA_KEY = "callTaxiAppData";

class Trip{
     constructor(){
         this._price=0
         this._routes=[]
         this._distance=0
         this._time=''
         this._taxiType=''
     }
    get start(){
        return this._routes[0]
    }
    get desination(){
        return this._routes[this._routes.length-1]
    }
    get price(){
        return this._price
    }
    get routes(){
        return this._routes
    }    
    get stops(){
        return this._routes.length-2
    }
    get time(){
       return this._time
    }
    get taxiType(){
        return this._taxiType
    }
    set distance(newDistance){
        this._distance=newDistance
    }
    set price(newPrice){
        this._price=newPrice
    }
    set time(newTime){
        this._time=newTime
    }
    set taxiType(newType){
        this._taxiType=newType
    }
    removeDestination(){
        this._routes.splice(this._routes.length-1,1)
    }
    getRouteCoord(){
        let routeCoord=[]
        for(let i=0;i<this._routes.length;i++){
            routeCoord.push(this._routes[i].coordinates)
        }
        return routeCoord
    }
    fromData(data){
        this._price=data._price
        this._routes=data._routes
        this._distance=data._distance
        this._time=data._time
    }
}
    
    
class Account{
    constructor(){
        this._tripList=[]
    }
    get numberOfTrips(){
        return this._tripList.length
    }
    addTrip(trip){
        this._tripList.push(trip)
    }
    removeTrip(index){
        this._tripList.splice(index,1)
    }
    getTrip(index){
        return this._tripList[index]
    }
    fromData(data){
        for(let i=0; i<data._tripList.length; i++){
            let newTrip = new Trip()
            newTrip.fromData(data._tripList[i])
            this._tripList.push(newTrip)
        }
    }
}

function checkLocalStorageDataExist(key){
    if(localStorage.getItem(key)){
        return true
    }
    return false
}
//this function is used to update data into localstorage
function updateLocalStorageData(key,data){
    let strData=JSON.stringify(data);
    localStorage.setItem(key,strData);
}
//this function is used to get data from localstorage
function getLocalStorageData(key){
    let jsonData = localStorage.getItem(key); 
    let data = jsonData;
    try
    {
        data = JSON.parse(jsonData); 
    }
    catch(e) {
        console.error(e); 
    }
    finally {
        return data;
    }
}
//global account instance variable
let tripList=new Account()
if(checkLocalStorageDataExist(APP_DATA_KEY)){
    tripList.fromData(getLocalStorageData(APP_DATA_KEY))
}
