//"use strict"
//This is main.js

//new class object
let trip=new Trip()

let taxiList = [
    {"rego":"VOV-887","type":"Car","available":true},
	{"rego":"OZS-293","type":"Van","available":false},
	{"rego":"WRE-188","type":"SUV","available":true},
	{"rego":"FWZ-490","type":"Car","available":true},
	{"rego":"NYE-874","type":"SUV","available":true},
	{"rego":"TES-277","type":"Car","available":false},
	{"rego":"GSP-874","type":"SUV","available":false},
	{"rego":"UAH-328","type":"Minibus","available":true},
	{"rego":"RJQ-001","type":"SUV","available":false},
	{"rego":"AGD-793","type":"Minibus","available":false}
];

let listTaxi=document.getElementById("taxiOption")

//create the taxi type data list 
for(let i=0;i<taxiList.length;i++){
  let option=document.createElement('option')
  option.textContent=taxiList[i].type
    option.value=taxiList[i].rego
  listTaxi.appendChild(option)
}

//URL of the geolocation 
 let URL = 'https://api.opencagedata.com/geocode/v1/json'
let key = '835326c5e3d843d6ba5bf82456316198'

mapboxgl.accessToken='pk.eyJ1IjoiamVycnl3ZWkiLCJhIjoiY2tzanI4anU0MDUwMDJubXRpd2xka3d6aCJ9.NUKoTBVodGow14Z9vig1UA'
    let caulfield = [145.0420733, -37.8770097];
    let map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom: 10,
        center: caulfield,    
    });

//this is a event when user click the map, it will call the function clickMap
map.on('click', (e) => {
//console.log(`A click event has occurred at ${e.lngLat}`);
    clickMap(e.lngLat)
})

//ask users for permission to share current location, if yes, call function success, if no, call function error
navigator.geolocation.getCurrentPosition(success,error)
 
//this function will be called when the user select yes on get current location, it should display a map and the center of the map is the current location of user
/*function success(currentLocation)
define a new map and the center is the current location
call webservice, the q of the keys are current location's latitude and longitude
the callback is displayMapInfo
display the current location marker
*/ 
function success(currentLocation){
    let latLng=currentLocation.coords.latitude +','+ currentLocation.coords.longitude
    map.panTo([currentLocation.coords.longitude,currentLocation.coords.latitude])
    let keys={
        'key':key,
        'q':latLng,
        'jsonp':'displayMapInfo'
    }
    webServiceRequest(URL,keys)
}

//this function will be called when user select no the get current location, and alert some error message.
function error(){
    alert('Geolocation not available')
}
//global variable to store markers
let markerList=[]

//this function is used to display all the information on the map contain marker and route. And each of  geting new location on this project need to call this function to display information on map. Moreover, this is a callback function, the input of this function is result, this result means the location that user select
/*
function displayMapInfo(result)
define a object to store location's coordinates and name
and this object need to be pushed into class instance trip.routes
use for loop to let the markerlist's marker to be undraggable except for the current one this is because  we only can drag the last marker
use mapbox method to display marker, the marker should contain a button called delete marker and there is a function when drag the marker, it will call the function adjust()
and then push the marker into marker list
and the use if statement if the length of the trip.routes > 1, we need to display the route line. And when trip.routes>2, we need to delete the route first, and then display the route line again. This is because when we add some location or make some changes, the past routes will affect new display routes
at the end, we need to get the html element of the display area, the location information need to be displayed
*/
function displayMapInfo(result){
    let locationInfo={
        coordinates:[result.results[0].geometry.lng,result.results[0].geometry.lat],
        name: result.results[0].formatted
    }
    trip.routes.push(locationInfo)//class instance
    for(let i=0;i<markerList.length-1;i++){
        markerList[i].draggable=false
    }
    let marker = new mapboxgl.Marker({
        draggable: true
        }).setLngLat(locationInfo.coordinates).setPopup(new mapboxgl.Popup().setHTML(locationInfo.name+'<br><button onclick="deleteMarker('+markerList.length+')">delete</button>')).addTo(map);
    marker.on('dragend', function(){
        adjust()
        })
    marker.getElement().addEventListener('mouseenter',function(){
        marker.togglePopup() 
    })
    markerList.push(marker)
    if (trip.routes.length>1){
        if(trip.routes.length>2){  
            map.removeLayer('locations')
            map.removeSource('locations')
        }
        map.addSource('locations', {
                
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    id: 'label',
                    'type': 'LineString',
                    'coordinates': 
                    trip.getRouteCoord()
                }
            }
        }
                     )
        
        map.addLayer({
            id:'locations',
            source:'locations',
            type:'line',
            layer:{
                type:'LineString'
            },
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 2
            }
        })      
    }
displayArea=document.getElementById('displayArea')
    let displayHtml='<h4>Locations:</h4>'
for(let i=0;i<trip.routes.length;i++){
    displayHtml+=trip.routes[i].name+'<br>'
}
    displayArea.innerHTML=displayHtml
}

//this function will be called when user drag the marker, it will receive the new location's information
/*
function adjust()
get the dragged marker new location 
and then delete the marker on the map also the markerlist
then use webservice to call displayMapInfo again
*/
function adjust(){
    let coord = markerList[markerList.length-1].getLngLat()
    markerList[markerList.length-1].remove()
    markerList.splice(markerList.length-1,1)
    trip.removeDestination()
    if(trip.routes.length===1){
        map.removeLayer('locations')
        map.removeSource('locations')
    }
    let latLng = coord.lat +','+ coord.lng
    let keys={
        'key':key,
        'q':latLng,
        'jsonp':'displayMapInfo'
    }
    webServiceRequest(URL,keys)
}

//this function is used to get the location information that user click the map. We add a event on the top that is map.on('click', (e) => {}clickMap(e.lngLat)}), so when we click the mao it will call this function
/*
function clickMap(newLocation)
define a variable that contains new location's lat and lng
call webservice and displayMapInfo
*/
function clickMap(newLocation){
    let latLng=newLocation.lat +','+newLocation.lng
    keys={
        'key':key,
        'q':latLng,
        'jsonp':'displayMapInfo'
    }
    webServiceRequest(URL,keys)
}

//this function is used to delete marker on the map, and when we delete the marker, the route and marker need to display again, bacause the routes also need to change
/*
function deleteMarker(markerIndex)
use a if statement to constrain user delete the first marker
use a for loop to delete all the marker on the map
use splice method to delete the marker on the markerlist
use a for loop to display all the marker after deleting
use splice method to delete the corresponding route in trip.routes
copy the display route from displayMapInfo function and the new locations are updated
as well as the displayArea, it also need to be updated when deleted one location
*/
function deleteMarker(markerIndex){
    //remove
    //call displayMarker 
    if(markerIndex===0){
        alert('You can not delete the first marker')
        return
    }
    for(let i=0;i<markerList.length;i++){
        markerList[i].remove()
    }
    markerList.splice(markerIndex,1)
    for(i=0;i<markerList.length;i++){
        markerList[i].addTo(map)
    }
    trip.routes.splice(markerIndex,1)
    map.removeLayer('locations')
    map.removeSource('locations')
    if (trip.routes.length>1){
        map.addSource('locations', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    id: 'label',
                    'type': 'LineString',
                    'coordinates': 
                    trip.getRouteCoord()
                }
            }
        }
                     )
        
        map.addLayer({
            id:'locations',
            source:'locations',
            type:'line',
            layer:{
                type:'LineString'
            },
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 2
            }
        })       
    }
    displayArea=document.getElementById('displayArea')
    let displayHtml='<h3>Locations:</h3>'
    for(let i=0;i<trip.routes.length;i++){
        displayHtml+=trip.routes[i].name+'<br>'
    }
    displayArea.innerHTML=displayHtml
}

//this function is used to calculate the distance between two points
/*
function calculationDis(lat1,lng1,lat2,lng2)
copy the fomular from the project summary
at the end we need to use math.round to make the result looking not too long
*/
function calculationDis(lat1,lng1,lat2,lng2){
const R = 6371; // metres
const R1 = lat1 * Math.PI/180; // φ, λ in radians
const R2 = lat2 * Math.PI/180;
const Tr = (lat2-lat1) * Math.PI/180;
const Tr2 = (lng2-lng1) * Math.PI/180;
const a = Math.sin(Tr/2) * Math.sin(Tr/2) +Math.cos(R1) * Math.cos(R2) *Math.sin(Tr2/2) * Math.sin(Tr2/2);
const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
const d = R * c;
return Math.round(d)
}

//this function is used to calculate the price of the trip
/*
function calculationPrice(d,taxiType,time)
define a object contains all the taxi type
copy the fomular from the project summary
different taxi type have different price, when the input meets the object, it return the object's value
use if statement to check the time of the trip, if the time is from 5pm to 9am, the price need to times 1.2
use math.round to make the result looking not so long 
*/
function calculationPrice(d,taxiType,time){
    let cost={
        'Car':0,
        'SUV':3.5,
        'Van':6,
        'Minibus':10
    }
    let flatRate=4.2
    let vehicleLevy=1.1
    let price=flatRate+vehicleLevy
    let distanceCost=1.622
    price+=d*distanceCost
    price+=cost[taxiType]
    let date=new Date(time)
    if(date.getHours()>17 && date.getHours()<9){
        price=price*1.2
    }
    return Math.round(price)
}

//this function will be called when user click the button confirm, it will check the input type and display the distance and price at the display area
/*
function confirmInfo()
get the input through html id
if statement to check the length of the routes, if it is less than 2, let user select unless 2 locations
else if statement check whether user select the taxi type
else if statement check whether user select the date
else if statement check whether the taxi is available 
else statement calculate the distance, need to use for loop to calculate all the distance between two points, and the conuter less than the trip.routes.length-1. then call the function calculationPrice to calculate the price of the trip
and the display the price and distance on the display area
and let the save's disabled be false
*/
function confirmInfo(){
    let list={}
    for(let i=0;i<taxiList.length;i++){
        list[taxiList[i].rego]=taxiList[i].available
    }
    let selectTaxi=document.getElementById('chooseTaxi')
    let selectDate=document.getElementById('tripDate')
    let displayArea=document.getElementById('displayArea')
    if(trip.routes.length<2){
        alert('You need to select one or more destinations')
    }
    else if(selectTaxi.value===''){
        alert('Please select the taxi type')
    }
    else if(selectDate===''){
        alert('Please select time of calling taxi')
    }
    else if(list[selectTaxi.value]===false){
        alert('This taxi is busy now, please change another taxi ')
    }
    else {
        let distance=0
        for(let i=0;i<trip.routes.length-1;i++){
            let lat1=trip.routes[i].coordinates[1]
            let lng1=trip.routes[i].coordinates[0]
            let lat2=trip.routes[i+1].coordinates[1]
            let lng2=trip.routes[i+1].coordinates[0]
            distance+=calculationDis(lat1,lng1,lat2,lng2)
        }
        let price=0
        for(let i=0; i<taxiList.length; i++){
            if (selectTaxi.value == taxiList[i].rego)
                price=calculationPrice(distance,taxiList[i].type,selectDate.value)
        }
        trip.distance=distance
        trip.price=price
        displayArea.innerText=displayArea.innerText+'total distance: '+distance+'km'+'\n'+'total price: $'+price+'\n'+'Stops:'+trip.stops
        document.getElementById('save').disabled=false
    }
}
//this function is used to save the trip information and update localstorage, then window location to view page
function saveInfo(){
    tripList.addTrip(trip)
    updateLocalStorageData(APP_DATA_KEY,tripList)
    updateLocalStorageData(TRIP_INDEX_KEY,tripList.numberOfTrips-1)
    alert('you have been saved your trip')
    window.location='view.html'
}
//this function is used to call webservice
function webServiceRequest(url,data)
{
	// Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}