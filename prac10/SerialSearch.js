/* Write your function for implementing the Serial Search Algorithm here */
function serialSearchTest(data){
    for (let i = 0; i<data.length; i++){
        if (data[i].emergency){
            return data[i].address
        }
    }
    return null
}